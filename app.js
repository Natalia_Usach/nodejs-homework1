const express = require('express');
const { request } = require('http');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const getFile = require('./modules/getFile');
const getAllFiles = require('./modules/getAllFiles');
const createFile = require('./modules/createFile');
// const deleteFile = require('./modules/deleteFile');
// const updateFile = require('./modules/updateFile');

const port = 8080;

app.listen(port, (error) => {
    error ? console.log(error) : console.log(`Listening port ${port}`)
});

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(express.json());
app.use(cors());

app.get('/api/files/:fileName', getFile);

app.get('/api/files', getAllFiles);

app.post('/api/files', createFile);

// app.delete('/api/files/:fileName', deleteFile);

// app.put('/api/files/:fileName', updateFile);