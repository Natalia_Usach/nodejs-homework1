const fs = require('fs');

const createFile = (req, res) => {
    const {filename, content} = req.body;
    const filesFolder = './files/';
    if (fs.existsSync(filesFolder)) {
        const filesList = [];
        fs.readdir(filesFolder, (error, files) => {
            files.forEach(file => {
                filesList.push(file);
            });
            if (!filename) {
                res.statusMessage = "Bad request";
                res.status(400).send({message: "Please specify 'filename' parameter"});
            } else if(!content) {
                res.statusMessage = "Bad request";
                res.status(400).send({message: "Please specify 'content' parameter"});
            } else if(filename && content && filesList.includes(filename)) {
                res.statusMessage = "Bad request";
                res.status(400).send({message: "File with such name already exists"});
            } else {
                res.statusMessage = "Success";
                res.status(200).send({message: 'File created successfully'});
                fs.writeFile(`${filesFolder}${filename}`, content ? content : '', (error) => {
                        
                });
            }
        });
    } else {
        res.statusMessage = "Internal server error";
        res.status(500).send({message: "Server error"});
    }
}

module.exports = createFile;