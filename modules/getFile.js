const fs = require('fs');

const getFile = (req, res) => {
        const filesFolder = './files/';
        if (fs.existsSync(filesFolder)) {
            const filesList = [];
                fs.readdir(filesFolder, (error, files) => {
                    try {
                        files.forEach(file => {
                            filesList.push(file);
                        });
                        if(filesList.includes(req.params.fileName)) {
                            fs.readFile(`${filesFolder}${req.params.fileName}`, (error, data) => {
                                fs.stat(`${filesFolder}${req.params.fileName}`, (err, stats) => {
                                    res.statusMessage = 'Success';
                                    res.status(200).send({message: 'Success', filename: req.params.fileName, content: data.toString(), extension: req.params.fileName.split('.').pop(), uploadedDate: stats.mtime});
                                });
                            });
                        } else {
                            throw error;
                        }
                    } catch(error) {
                        res.statusMessage = 'Bad request';
                        res.status(400).send({message: `No file with '${req.params.fileName}' filename found`});
                    }
                });
        } else {
            res.statusMessage = 'Server error';
            res.status(500).send({message: 'Server error'});
        }
    }

module.exports = getFile;