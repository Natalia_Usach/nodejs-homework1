const fs = require('fs');

const getAllFiles = (req, res) => {
    const filesFolder = './files/';
    if (fs.existsSync(filesFolder)) {
        const filesList = [];
        try {
                fs.readdir(filesFolder, (error, files) => {
                    files.forEach(file => {
                        filesList.push(file);
                    });
                    res.statusMessage = "Success";
                    res.status(200).send({message: 'Success', files: filesList.filter((file) => file !== '.gitkeep')});
                });
        } catch(error) {
            res.statusMessage = "Bad request";
            res.status(400).send({message: 'Client error'});
        }
    } else {
        res.statusMessage = "Internal server error";
        res.status(500).send({message: "Server error"});
    }
}

module.exports = getAllFiles;
